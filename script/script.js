"use strict";
debugger;

function addLiOnPage(array, parent = document.body) {
    let ulElement = document.createElement("ul");
    // let InnerUlElement = document.createElement("ul");  // This line is for "FOR" and "MAP"
    /*FOR*/
    /*for (let i = 0; i < array.length; i++) {
        if (!Array.isArray(array[i])) {
            let arrayElementToList = document.createElement("li");
            arrayElementToList.innerText = array[i];

            ulElement.append(arrayElementToList);
        } else if (Array.isArray(array[i])) {
            let nestedArr = array[i];

            for (let j = 0; j < nestedArr.length; j++) {
                let InnerArrayElementToList = document.createElement("li");
                InnerArrayElementToList.innerText = nestedArr[j];

                InnerUlElement.append(InnerArrayElementToList);
                ulElement.append(InnerUlElement);
            }
        }
    }*/

    /*MAP*/
    /*let getArray = () => {
        array.map((elem) => {
            if (!Array.isArray(elem)) {
                let arrayElementToList = document.createElement("li");
                arrayElementToList.innerText = elem;

                ulElement.append(arrayElementToList);

            } else if (Array.isArray(elem)) {
                elem.map((innerElem) => {

                    let InnerArrayElementToList = document.createElement("li");
                    InnerArrayElementToList.innerText = innerElem;

                    InnerUlElement.append(InnerArrayElementToList);
                    ulElement.append(InnerUlElement);
                })
            }
            parent.append(ulElement);
        })
    }
    getArray();*/

    /*MAP+RECURSION*/
    array.map((arrayElement) => {
        let arrayElementToList = document.createElement("li");
        if (Array.isArray(arrayElement)) {
            arrayElementToList.style.listStyle = "none";
            addLiOnPage(arrayElement, arrayElementToList);
        } else {
            arrayElementToList.innerText = arrayElement;
        }
        ulElement.append(arrayElementToList);
    });
    parent.append(ulElement);
}

const timer = () => {
    let counter = 4;
    const paragraphTimer = document.createElement("p");
    document.body.append(paragraphTimer);
    document.body.querySelector('p').style.textAlign = 'center';
    document.body.querySelector('p').style.fontSize = '25px';
    document.body.querySelector('p').style.color = 'blue';

    const idInterval = setInterval(() => {
        counter--;
        if (counter === 0) {
            clearInterval(idInterval);
            document.body.outerHTML = "";
        }
        paragraphTimer.textContent = `Контент видалиться через ${counter} секунди`;
    }, 1000);
};

timer();

console.log(addLiOnPage(["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"], document.querySelector('body')));
console.log(addLiOnPage(["1", "2", "3", "sea", "user", 23], document.querySelector('body')));
console.log(addLiOnPage(["Kharkiv", "Kiev", ["Borispol", "Irpen"], "Odessa", "Lviv", "Dnieper"], document.querySelector('body')));
console.log(addLiOnPage(["Kharkiv", "Kiev", ["Borispol", [["Borispol", ["Odessa", "Lviv", ["Odessa", "Lviv", "Dnieper"]]], "Lviv", "Dnieper"]], "Odessa", "Lviv", "Dnieper"], document.querySelector('body')));
